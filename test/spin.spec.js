describe('Spinning the slots...', () => {

  test('High chance', () => {
    expect(Math.random()).toBeLessThanOrEqual(0.9);
  });

  test('Mid chance', () => {
    expect(Math.random()).toBeLessThanOrEqual(0.6);
  });

  test('Low chance', () => {
    expect(Math.random()).toBeLessThanOrEqual(0.3);
  });

});
