const TreeDirector = require('./tree');

describe('Planting a tree...', () => {

  const seed = Math.random();
  const tree = TreeDirector.plant(seed);

  test('It has the same seed', () => {
    expect(tree.seed).toBe(seed);
  });

  test('It has sprouts', () => {
    expect(tree.sprouts).toBeTruthy();
  });
    
  test('It has a trunk', () => {
    expect(tree.trunk).toBeTruthy();
  });
    
  test('It has branches', () => {
    expect(tree.branches).toBeTruthy();
  });
    
  test('It has leaves', () => {
    expect(tree.leaves).toBeTruthy();
  });

});
