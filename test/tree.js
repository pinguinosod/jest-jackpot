class Tree {
  constructor(builder) {
    this.seed = builder.seed;
    this.sprouts = builder.sprouts ? builder.sprouts : false;
    this.trunk = builder.trunk ? builder.trunk : false;
    this.branches = builder.branches ? builder.branches : false;
    this.leaves = builder.leaves ? builder.leaves : false;
  }
}

Tree.builder = class TreeBuilder {
  withSeed(seed) {
    this.seed = seed;
    return this;
  }

  withSprouts() {
    this.sprouts = true;
    return this;
  }

  withTrunk() {
    this.trunk = true;
    return this;
  }

  withBranches() {
    this.branches = true;
    return this;
  }

  withLeaves() {
    this.leaves = true;
    return this;
  }

  build() {
    return new Tree(this);
  }
}

class TreeDirector {
  static plant(seed) {
    console.log('planting a tree with seed: ' + seed);
    const treeBuilder = new Tree.builder();
    treeBuilder.withSeed(seed);
    if (seed >= 0.2) treeBuilder.withSprouts();
    if (seed >= 0.4) treeBuilder.withTrunk();
    if (seed >= 0.6) treeBuilder.withBranches();
    if (seed >= 0.8) treeBuilder.withLeaves();
    return treeBuilder.build();
  }
}

module.exports = TreeDirector;
